using UnityEngine;

public class Trampa : MonoBehaviour
{
    public float velocidad = 5.0f; 
    public Transform puntoInicio;
    public Transform puntoFinal;

    private Rigidbody rb;
    private Vector3 objetivo;

    void Start()
    {
        rb = GetComponent<Rigidbody>();
        objetivo = puntoFinal.position;
    }

    void Update()
    {
        
        Vector3 direccion = (objetivo - transform.position).normalized;
        Vector3 movimiento = direccion * velocidad * Time.deltaTime;

        
        rb.MovePosition(transform.position + movimiento);

        
        if (Vector3.Distance(transform.position, objetivo) < 0.1f)
        {
            CambiarObjetivo();
        }
    }

    void CambiarObjetivo()
    {
        
        objetivo = (objetivo == puntoInicio.position) ? puntoFinal.position : puntoInicio.position;
    }
}
