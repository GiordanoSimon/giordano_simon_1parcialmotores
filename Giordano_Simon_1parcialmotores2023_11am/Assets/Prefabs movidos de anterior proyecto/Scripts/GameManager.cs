using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    public float tiempoRestante;
    public GameObject jugador;
    public GameObject bot;
    public GameObject FinDelJuego;
    private List<GameObject> listaEnemigos = new List<GameObject>();
    private bool Pausado = false;
    private float tiempoInicial;

    void Start()
    {
        ComenzarJuego();
    }

    void Update()
    {
        if (!Pausado)
        {
            if (jugador.transform.position.y < -1)
            {
                Findeljuego();
            }

            if (tiempoRestante <= 0)
            {
                Findeljuego();
            }
        }
    }

    void ComenzarJuego()
    {
        tiempoInicial = tiempoRestante;
        jugador.transform.position = new Vector3(3.458004f, 0.63f, -2.37f);

        foreach (GameObject item in listaEnemigos)
        {
            Destroy(item);
        }

        listaEnemigos.Clear();
        StartCoroutine(ComenzarCronometro(tiempoRestante));
    }

    public void Ganaste()
    {
        FinDelJuego.SetActive(true);
        Pausado = true;
        Time.timeScale = 0;
    }

    public void Findeljuego()
    {
        FinDelJuego.SetActive(true);
        Pausado = true;
        Time.timeScale = 0;
    }

    public void ResetTiempo()
    {
        tiempoRestante = tiempoInicial;
        Pausado = false;
        Time.timeScale = 1.0f;
    }

    public IEnumerator ComenzarCronometro(float valorCronometro = 30)
    {
        tiempoRestante = valorCronometro;
        while (tiempoRestante > 0)
        {
            Debug.Log("Quedan " + tiempoRestante + " segundos.");
            yield return new WaitForSeconds(1.0f);
            tiempoRestante--;
        }
    }
}
