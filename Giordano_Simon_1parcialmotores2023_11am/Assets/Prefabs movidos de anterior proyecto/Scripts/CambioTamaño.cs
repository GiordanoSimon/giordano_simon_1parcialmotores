using UnityEngine;

public class CambioTamaño : MonoBehaviour
{
    public float alturaInicial = 0f;
    public float velocidadInicial = 1.0f;
    public float aumentoAltura = 2.0f;
    public float aumentoVelocidad = 2.0f;
    public GameObject objetoEspecifico;  
    private bool agrandado = false;

    void Start()
    {
        transform.localScale = new Vector3(1, alturaInicial, 1);
    }

    void Update()
    {
        if (!agrandado && objetoEspecifico != null)
        {
            
            if (Physics.Raycast(transform.position, transform.forward, out RaycastHit hit) &&
                hit.collider.gameObject == objetoEspecifico)
            {
                
                AplicarCrecimiento();
                agrandado = true;

                
                objetoEspecifico.SetActive(false);
            }
        }
    }

    public void AplicarCrecimiento()
    {
        alturaInicial += aumentoAltura;
        velocidadInicial += aumentoVelocidad;

        Vector3 nuevaEscala = transform.localScale;
        nuevaEscala.y = alturaInicial;
        transform.localScale = nuevaEscala;
    }
}
