using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnBots : MonoBehaviour
{
    public GameObject botPrefab;
    public Transform spawnPoint;
    public float SpawnDelay = 3.0f;
    public int MaximoSpawn = 10;

    private int currentSpawnCount = 0;
    private float timeSinceLastSpawn = 0.0f;
    private bool Jugadorenrango = false;

    void Update()
    {
        if (Jugadorenrango && currentSpawnCount < MaximoSpawn)
        {
            timeSinceLastSpawn += Time.deltaTime;

            if (timeSinceLastSpawn >= SpawnDelay)
            {
                SpawnBot();
                timeSinceLastSpawn = 0.0f;
            }
        }
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            Jugadorenrango = true;
        }
    }

    void OntriggerExit(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            Jugadorenrango = false;
        }
    }

    void SpawnBot()
    {
        if (botPrefab != null)
        {
            Instantiate(botPrefab, spawnPoint.position, spawnPoint.rotation);
            currentSpawnCount++;
        }
    }
}
