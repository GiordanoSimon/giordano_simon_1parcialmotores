using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Temporizador : MonoBehaviour
{
    public Text textoTemporizador;

    private float tiempoRestante = 190.0f;

    void Start()
    {

        ActualizarTextoTemporizador();
    }

    void Update()
    {

        if (tiempoRestante > 0)
        {
            tiempoRestante -= Time.deltaTime;
            ActualizarTextoTemporizador();
        }
    }

    void ActualizarTextoTemporizador()
    {

        textoTemporizador.text = "Tiempo: " + Mathf.Round(tiempoRestante).ToString();
    }
}
